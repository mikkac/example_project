#define CATCH_CONFIG_MAIN
#include "catch2/catch.hpp"
#include "lib.h"
#include "spdlog/spdlog.h"

TEST_CASE("AddTwoTestCase", "[add_two]")
{
    spdlog::info("test");
    REQUIRE(lib::add_two(1) == 3);
    REQUIRE(lib::add_two(100) == 102);
    REQUIRE(lib::add_two(-2) == 0);
    REQUIRE(lib::add_two(0) == 2);
}
