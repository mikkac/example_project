enable_testing()

set(tests_dir ${PROJECT_SOURCE_DIR}/tests)
set(tests_target ${PROJECT_NAME}_tests)

set(tests_src
    ${tests_dir}/lib_tests.cpp
)

add_executable(${tests_target} ${tests_src})

target_include_directories(${tests_target} PUBLIC ${PROJECT_SOURCE_DIR}/app/inc)

target_link_libraries(${tests_target}
    ${lib}
    ${CONAN_LIBS_CATCH2}
    ${CONAN_LIBS_SPDLOG}
    ${CONAN_LIBS_FMT}
)
