set(lib_src
    ${CMAKE_SOURCE_DIR}/app/src/lib.cpp
)

set(lib_inc
    ${CMAKE_SOURCE_DIR}/app/inc/lib.h
)

set(lib "lib")
add_library(${lib} ${lib_src})
target_include_directories(${lib} PUBLIC app/inc)

add_executable(${PROJECT_NAME} app/main.cpp)

target_link_libraries(${PROJECT_NAME} ${lib} ${CONAN_LIBS_FMT} ${CONAN_LIBS_SPDLOG}) 
