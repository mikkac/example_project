#include "lib.h"
#include "spdlog/spdlog.h"

int main(int argc, char *argv[])
{
    auto res = lib::add_two(5);
    spdlog::info("result of add_two(5) is equal to {}", res);
    return 0;
}
