#include "lib.h"

namespace lib
{

int add_two(int num)
{
    return num + 2;
}

} // namespace lib
